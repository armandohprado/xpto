import { Cep } from "./cep.model";

export interface Cliente {
  id?: number;
  nome?: string;
  email?: string;
  cpf?: string;
  Endereco?: Cep;
}
