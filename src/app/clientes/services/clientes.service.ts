import { Cep } from "src/app/clientes/models/cep.model";
import { Injectable } from "@angular/core";
import { Cliente } from "../models/cliente.model";

@Injectable({
  providedIn: "root"
})
export class ClientesService {
  cliente: Cliente;
  constructor() {}
  listarTodos(): Cliente[] {
    const clientes = localStorage["clientes"];
    return clientes ? JSON.parse(clientes) : [];
  }

  cadastrar(cliente: Cliente, endereco: Cep): void {
    cliente.Endereco = endereco;
    const clientes = this.listarTodos();
    cliente.id = new Date().getTime();
    clientes.push(cliente);
    localStorage["clientes"] = JSON.stringify(clientes);
  }
  remover(id: number): boolean {
    let clientes: Cliente[] = this.listarTodos();
    clientes = clientes.filter(cliente => cliente.id !== id);
    localStorage["clientes"] = JSON.stringify(clientes);
    return !!clientes;
  }
  buscarId(id: number): Cliente[] {
    let cliente: Cliente[] = this.listarTodos();
    cliente = cliente.filter(cliente => cliente.id == id);
    return cliente;
  }
}
