import { ClientesService } from "./../../services/clientes.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-visualizar",
  templateUrl: "./visualizar.component.html",
  styleUrls: ["./visualizar.component.scss"]
})
export class VisualizarComponent implements OnInit, OnDestroy {
  cliente;
  constructor(
    private clientesService: ClientesService,
    private routeActive: ActivatedRoute
  ) {}

  ngOnInit() {
    this.routeActive.params.subscribe(
      cliente =>
        (this.cliente = this.clientesService.buscarId(cliente.idCliente)[0])
    );
  }

  ngOnDestroy(): void {}
}
