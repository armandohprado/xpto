import { Cliente } from "./../../models/cliente.model";
import { ClientesService } from "./../../services/clientes.service";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-listar",
  templateUrl: "./listar.component.html",
  styleUrls: ["./listar.component.scss"]
})
export class ListarComponent implements OnInit {
  clientes: Cliente[];
  router: any;
  constructor(private clienteService: ClientesService, private route: Router) {}

  ngOnInit() {
    this.clientes = this.listarTodos();
  }

  listarTodos(): Cliente[] {
    return this.clienteService.listarTodos();
  }
  remover(id: number, $event): void {
    if (confirm("deseja remover ? ")) {
      this.clienteService.remover(id);
      // this.route.navigate([""]);
      window.location.reload();
    }
  }
}
