import { Component, OnInit, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Cep } from "src/app/clientes/models/cep.model";
import { CepService } from "src/app/clientes/services/cep.service";

@Component({
  selector: "app-endereco",
  templateUrl: "./endereco.component.html",
  styleUrls: ["./endereco.component.scss"]
})
export class EnderecoComponent implements OnInit {
  @Input("endereco") formClienteEndereco: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private cepService: CepService
  ) {}
  consultaCEP(cep) {
    cep = cep.replace(/\D/g, "");
    if (!!cep) {
      this.cepService
        .consultaCEP(cep)
        .subscribe(dados => this.populaDadosForm(dados));
    }
  }
  populaDadosForm(dados) {
    this.formClienteEndereco.setValue({
      logradouro: dados.logradouro,
      cep: dados.cep,
      numero: "",
      complemento: dados.complemento,
      bairro: dados.bairro,
      cidade: dados.localidade,
      estado: dados.uf
    });

    // console.log(form);
  }
  ngOnInit() {}
}
