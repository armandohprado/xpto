import { ClientesService } from "./../../services/clientes.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { Cliente } from "../../models/cliente.model";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-cadastrar",
  templateUrl: "./cadastrar.component.html",
  styleUrls: ["./cadastrar.component.scss"]
})
export class CadastrarComponent implements OnInit {
  formCliente: FormGroup = this.formBuilder.group({
    nome: this.formBuilder.control("", Validators.required),
    cpf: this.formBuilder.control("", Validators.required),
    email: this.formBuilder.control("", Validators.required)
  });
  formClienteEndereco: FormGroup = this.formBuilder.group({
    cep: this.formBuilder.control("", Validators.required),
    logradouro: this.formBuilder.control("", Validators.required),
    numero: this.formBuilder.control("", Validators.required),
    complemento: this.formBuilder.control("", Validators.required),
    cidade: this.formBuilder.control("", Validators.required),
    bairro: this.formBuilder.control("", Validators.required),
    estado: this.formBuilder.control("", Validators.required)
  });

  cliente: Cliente;

  constructor(
    private router: Router,
    private clienteService: ClientesService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {}
  cadastrar(): void {
    // tslint:disable-next-line: prefer-const
    this.clienteService.cadastrar(
      this.formCliente.value,
      this.formClienteEndereco.value
    );
    this.router.navigate([""]);
  }
  cancelar(): void {
    this.router.navigate([""]);
  }
}
