import { CadastrarComponent } from "./components/cadastrar/cadastrar.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ListarComponent } from "./components/listar/listar.component";
import { VisualizarComponent } from "./components/visualizar/visualizar.component";

const routes: Routes = [
  { path: "clientes/listar", component: ListarComponent },
  { path: "", redirectTo: "/clientes/listar", pathMatch: "full" },
  { path: "clientes/cadastrar", component: CadastrarComponent },
  { path: "clientes/visualizar/:idCliente", component: VisualizarComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientesRoutingModule {}
