import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ClientesRoutingModule } from "./clientes-routing.module";
import { CadastrarComponent } from "./components/cadastrar/cadastrar.component";
import { ListarComponent } from "./components/listar/listar.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CPFMask } from "cpf-mask-ng2";
import { EnderecoComponent } from "./components/cadastrar/endereco/endereco.component";
import { VisualizarComponent } from "./components/visualizar/visualizar.component";

@NgModule({
  declarations: [
    CadastrarComponent,
    ListarComponent,
    CPFMask,
    EnderecoComponent,
    VisualizarComponent
  ],
  imports: [
    CommonModule,
    ClientesRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [FormsModule, ReactiveFormsModule, CPFMask]
})
export class ClientesModule {}
